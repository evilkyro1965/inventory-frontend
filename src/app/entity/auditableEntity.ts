import { LookupEntity } from './lookupEntity';

export class AuditableEntity extends LookupEntity {
  
  public createdBy: string;
  public cretedDate: Date;
  public updatedBy: string;
  public updatedDate: Date;

  constructor(
    public id: number,
    public name: string
    ) 
  { 
  	super(id,name);
  }

}
