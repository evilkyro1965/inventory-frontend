import { IdentifiableEntity } from './identifiableEntity';
import { Product, Measurement, ReceiveDetails } from './entities';

export class OrderDetails extends IdentifiableEntity {

  public totalPrice : number;
  public discountTotal : number;

  public receiveDetails : ReceiveDetails;

  constructor(
  	public id: number,
    public purchaseId: number,
    public product: Product,

    public quantityUOM: number,
    public quantityUOMStr: string,

    public quantity : number,
    public measurementLabel : string,
    public usePurchaseUOM : boolean,
    public purchaseUOMConversion : number,
    public unitPrice: number,
    public unitPriceStr : string,

    public discount: number,
    public discountStr: string,

    public subTotal: number,
    public subTotalStr : string
    ) 
  { 
  	super(id);
  }
}
