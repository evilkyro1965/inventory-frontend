import { IdentifiableEntity } from './identifiableEntity';
import { Product, Measurement, Location, OrderDetails } from './entities';

export class ReceiveDetails extends IdentifiableEntity {

  public totalPrice : number;
  public discountTotal : number;

  constructor(
  	public id: number,
    public purchaseId: number,
    public product: Product,
    public location: Location,
    public orderDetails : OrderDetails,

    public quantityUOM: number,
    public quantityUOMStr: string,

    public quantity : number,

    public usePurchaseUOM : boolean,
    public purchaseUOMConversion : number,

    public receiveDate : Date,

    public orderQty : number,
    public orderQtyStr : string
    ) 
  { 
  	super(id);
  }
}
