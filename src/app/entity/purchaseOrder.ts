import { IdentifiableEntity } from './identifiableEntity';
import { OrderDetails, Vendor } from './entities';

export class PurchaseOrder extends IdentifiableEntity {

  public no: string;
  public date: Date;  
  public dueDate: Date;  
  public orders: OrderDetails[];  
  public taxPercent: number;
  public taxTotal : number;
  public total : number;
  public freight : number;
  public vendor : Vendor;
  public paid : boolean;
  public closed : boolean;
  public fulfilled : boolean;
  public receiving : boolean;

  constructor(
  	public id: number,
    ) 
  { 
  	super(id);
  }
}
