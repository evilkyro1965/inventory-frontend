import { ActiveAuditEntity } from './activeAuditEntity';

export class Measurement extends ActiveAuditEntity {

  public createdBy: string;
  public createdDate: Date;
  public updatedBy: string;
  public updatedDate: Date;
  

  constructor(
  	public id: number,
    public name: string,
    public abbreviation: string,
    public isActive: boolean,
    ) 
  { 
  	super(id,name,isActive);
  }
}
