import { ActiveAuditEntity } from './activeAuditEntity';

export class Province extends ActiveAuditEntity {

  public createdBy: string;
  public createdDate: Date;
  public updatedBy: string;
  public updatedDate: Date;

  constructor(
  	public id: number,
    public name: string,
    public isActive: boolean,
    ) 
  { 
  	super(id,name,isActive);
  }
}
