import { SearchFilterParam } from './searchFilterParam';
import { SortType } from './entities';

export class SearchCriteria {
  public page: number = 1;
  public limit: number = 1;
  public pageCount: number;
  
  public totalRow : number = 0;
  public totalPage : number = 1;
  public arrPage : number[] = [1];

  public sortBy : string = "";
  public sortType : SortType = "asc";

  public searchParams : SearchFilterParam[] = [];

  public name: string = "";

  constructor() 
  { 
  	
  }
}
