import { ActiveAuditEntity } from './activeAuditEntity';
import { Address } from './entities';

export class Vendor extends ActiveAuditEntity {

  public createdBy: string;
  public createdDate: Date;
  public updatedBy: string;
  public updatedDate: Date;
  
  public address: Address;

  constructor(
  	public id: number,
    public name: string,
    public phone: string,
    public fax: string,
    public email: string,
    public website: string,
    public remarks: string,
    public isActive: boolean,
    ) 
  { 
  	super(id,name,isActive);
  }
}
