import { IdentifiableEntity } from './identifiableEntity';
import { City, Province, Country } from './entities';

export class Address extends IdentifiableEntity {

  public createdBy: string;
  public createdDate: Date;
  public updatedBy: string;
  public updatedDate: Date;
  
  constructor(
  	public id: number,
    public street: string,
    public city: City,
    public province: Province,
    public country: Country,
    public postalCode: string,
    public remarks: string
    ) 
  { 
  	super(id);
  }
}
