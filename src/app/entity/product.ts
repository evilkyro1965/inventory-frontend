import { ActiveAuditEntity } from './activeAuditEntity';
import { Measurement, Location, ProductCategory } from './entities';

export class Product extends ActiveAuditEntity {

  public createdBy: string;
  public createdDate: Date;
  public updatedBy: string;
  public updatedDate: Date;

  public category : ProductCategory;
  public code : string;
  public description : string;
  public barcode : string;
  public reorderPoint : number;
  public remarks : string;
  public defaultLocation : Location;
  public standardUOM : Measurement;
  public salesUOM : Measurement;
  public salesUOMConversion : number;
  public purchaseUOM : Measurement;
  public purchaseUOMConversion : number;
  public costPrice : number;
  public normalSalesPrice : number;
  public normalPurchaseSales : number;

  constructor(
  	public id: number,
    public name: string,
    public isActive: boolean,
    ) 
  { 
  	super(id,name,isActive);
  }
}
