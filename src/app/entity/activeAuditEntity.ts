import { AuditableEntity } from './auditableEntity';

export class ActiveAuditEntity extends AuditableEntity {

  constructor(
  	public id: number,
    public name: string,
    public isActive: boolean
    ) 
  { 
  	super(id,name);
  }
}
