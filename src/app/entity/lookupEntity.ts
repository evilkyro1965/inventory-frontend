import { IdentifiableEntity } from './identifiableEntity';

export class LookupEntity extends IdentifiableEntity {
  constructor(
  	public id: number,
    public name: string
    ) 
  { 
  	super(id);
  }
}
