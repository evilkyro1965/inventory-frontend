import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Vendor } from '../entity/entities';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class VendorService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchVendors(searchParams : SearchFilterParam[], page : number, perPage : number, sortBy : string, sortType : SortType) {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    params.append("_page",page.toString());
	    params.append("_limit",perPage.toString());

	    if(sortBy!=null) {
	    	params.append("_sort",sortBy.toString());
	    	params.append("_order",sortType.toString());
	    }

	    for (let index in searchParams) {
		    let param : SearchFilterParam = searchParams[index];
		    params.append(param.field,param.value);
		}
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/vendors', { search: params, headers: this.headers });
	}

	createVendor(vendor: Vendor) {

		let body : any = this.toJson(vendor);

		return this.http.post(this.globalVariable.apiBaseUrl+'/vendors/',
				body,
				{headers: this.headers}
			);
	}

	saveVendor(vendor: Vendor) {

		let body : any = this.toJson(vendor);

		return this.http.put(this.globalVariable.apiBaseUrl+'/vendors/'+vendor.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(vendor: Vendor) {
		let body : any = {};
		body.id = vendor.id;
		body.name = vendor.name;
		body.phone = vendor.phone;
		body.fax = vendor.fax;
		body.email = vendor.email;
		body.website = vendor.website;
		body.remarks = vendor.remarks;
		body.isActive = vendor.isActive;

		var address : any = {};
		address.street = vendor.address.street;
		address.city = vendor.address.city;
		address.province = vendor.address.province;
		address.country = vendor.address.country;
		address.postalCode = vendor.address.postalCode;
		address.remarks = vendor.address.remarks;

		body.address = address;

		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}