import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { SharedGlobalModule } from '../shared/shared.module';

import { VendorComponent }  from './vendor.component';

import { VendorRouting }   from './vendor.routing';

import { VendorService } from './vendor.service';

import {ButtonModule,
	InputTextModule,
  GrowlModule,
	FieldsetModule,
  PanelModule,
	TabViewModule,
	DropdownModule,
  DialogModule,
	SharedModule,
	DataTableModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
          FormsModule,
          SharedGlobalModule,
  				VendorRouting,
  				ButtonModule,
  				InputTextModule,
          GrowlModule,
  				FieldsetModule,
          PanelModule,
  				TabViewModule,
  				DropdownModule,
          DialogModule,
  				SharedModule,
  				DataTableModule 
  				],
  declarations: [ VendorComponent],
  providers : [ VendorService ]
})
export class VendorModule {}