import { Component, OnInit } from '@angular/core';

import { VendorService } from './vendor.service';
import { CityService } from '../city/city.service';
import { ProvinceService } from '../province/province.service';
import { CountryService } from '../country/country.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Vendor, Address, City, Province, Country, SearchFilterParam, SortType, SearchCriteria } from '../entity/entities';

import {SelectItem,Message} from 'primeng/primeng';

import * as _ from 'lodash'

@Component({
	selector: 'vendor',
	templateUrl: './vendor.component.html',
	providers: [ VendorService ]
})
export class VendorComponent implements OnInit { 

	searchCriteria : SearchCriteria = new SearchCriteria();
  
	nameFilter: string = "";

  sortFields : SelectItem[] = [];

	vendors: Vendor[];
  cities: SelectItem[] = [];
  provinces: SelectItem[] = [];
  countries: SelectItem[] = [];
  
  collapseFilter : boolean = true;

  displayDialog : boolean = false;
  isSaving : boolean = false;
  isEditing : boolean = false;

  vendor : Vendor;

  msgs : Message[] = [];

	constructor(private vendorService: VendorService, 
    private cityService: CityService, 
    private provinceService: ProvinceService,
    private countryService: CountryService, 
    public globalVariable : GlobalVariableComponent) {

    this.cityService.searchCities()
      .subscribe(
        result => {
          let cities = <City[]> result.json();
          this.cities = this.globalVariable.entityToSelectItems(cities,"- City -");
        }
      );
    this.provinceService.searchProvinces()
      .subscribe(
        result => {
          let provinces = <Province[]> result.json();
          this.provinces = this.globalVariable.entityToSelectItems(provinces,"- Province -");
        }
      ); 
    this.countryService.searchCountries()
      .subscribe(
        result => {
          let countries = <Country[]> result.json();
          this.countries = this.globalVariable.entityToSelectItems(countries,"- Country -");
        }
      );       
	}	

	ngOnInit(): void {
      this.searchCriteria.sortBy = "name";
      this.searchCriteria.sortType = "asc";

    	this.getTableData();
  	}

  	getTableData() {
  		this.vendorService.searchVendors(this.searchCriteria.searchParams,this.searchCriteria.page,
    		this.searchCriteria.limit,this.searchCriteria.sortBy,this.searchCriteria.sortType)
    		.subscribe(result => {
    			 this.vendors = <Vendor[]> result.json();
    			 this.searchCriteria.totalRow = parseInt(result.headers.get("X-Total-Count"));
    			 this.searchCriteria.totalPage = this.globalVariable.getTotalPage(this.searchCriteria.totalRow,this.searchCriteria.limit);
    			 this.searchCriteria.arrPage = this.globalVariable.getPageArray(this.searchCriteria.page,this.searchCriteria.totalPage);
    		});
  	}

  	getPageData(page:number) {
  		this.searchCriteria.page = page;
  		this.getTableData();
  	}

  	filterTableData() {
  		this.searchCriteria.page = 1;
  		this.searchCriteria.searchParams = [];

      
  		if(this.nameFilter!="") {
  			this.searchCriteria.searchParams.push(new SearchFilterParam("name_like",this.nameFilter));
  		}

      this.collapseFilter = true;
  		this.getTableData();
  	}

    resetForm() {
      this.isEditing = false;
      this.isSaving = false;
      this.vendor = null;
    }

    viewRow(rowData: Vendor) {
      this.resetForm();
      this.vendor = _.cloneDeep(rowData);
      this.displayDialog = true;
    }

    addRow() {
      this.resetForm();
      this.vendor = new Vendor(null,'','','','','','',true);
      this.vendor.address = new Address(null,'',null,null,null,'','');
      this.isSaving = true;
      this.displayDialog = true;
    }

    editRow(rowData: Vendor) {
      this.resetForm();
      this.vendor = _.cloneDeep(rowData);
      this.isEditing = true;
      this.displayDialog = true;
    }

    closeModal() {
      this.resetForm();
      this.displayDialog = false;
    }

    update() {

      //Validation

      let isValid : boolean = true;

      if(
        this.vendor.name.trim() == ''
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.vendorService.saveVendor(this.vendor)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.filterTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }


    }

    save() {

      //Validation

      let isValid : boolean = true;

      if(
        this.vendor.name.trim() == ''
        )
      {
       isValid = false; 
      }   

      if(isValid) {

        this.vendorService.createVendor(this.vendor)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Creating Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.filterTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }
     
    }

}

