import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { ProvinceComponent }   from './province.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'province', component: ProvinceComponent}
  ])],
  exports: [RouterModule]
})
export class ProvinceRouting {}