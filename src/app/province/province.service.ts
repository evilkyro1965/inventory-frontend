import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Province } from '../entity/province';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class ProvinceService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchProvinces() {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/provinces', { search: params, headers: this.headers });
	}

	createProvince(province: Province) {
		let body : any = this.toJson(province);

		return this.http.post(this.globalVariable.apiBaseUrl+'/provinces/',
				body,
				{headers: this.headers}
			);
	}

	saveProvince(province: Province) {
		let body : any = this.toJson(province);

		return this.http.put(this.globalVariable.apiBaseUrl+'/provinces/'+province.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(province: Province) {
		let body : any = {};
		body.id = province.id;
		body.name = province.name;
		body.isActive = province.isActive;
		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}