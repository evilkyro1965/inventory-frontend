import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { SharedGlobalModule }        from '../shared/shared.module';

import { ProvinceComponent }  from './province.component';

import { ProvinceRouting }   from './province.routing';

import { ProvinceService } from './province.service';

import {ButtonModule,
	InputTextModule,
  GrowlModule,
	FieldsetModule,
  PanelModule,
	TabViewModule,
	DropdownModule,
  DialogModule,
	SharedModule,
	DataTableModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
          FormsModule,
          SharedGlobalModule,
  				ProvinceRouting,
  				ButtonModule,
  				InputTextModule,
          GrowlModule,
  				FieldsetModule,
          PanelModule,
  				TabViewModule,
  				DropdownModule,
          DialogModule,
  				SharedModule,
  				DataTableModule 
  				],
  declarations: [ ProvinceComponent ],
  providers : [ ProvinceService ]
})
export class ProvinceModule {}