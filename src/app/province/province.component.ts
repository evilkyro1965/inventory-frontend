import { Component, OnInit } from '@angular/core';

import { ProvinceService } from './province.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Province } from '../entity/entities';

import { SelectItem, Message } from 'primeng/primeng';

@Component({
	selector: 'province',
	templateUrl: './province.component.html',
	providers: [ ProvinceService ]
})
export class ProvinceComponent implements OnInit { 

	cities: Province[];

  displayDialog : boolean = false;
  isSaving : boolean = false;
  isEditing : boolean = false;

  city : Province;

  msgs : Message[] = [];

	constructor(private provinceService: ProvinceService, public globalVariable : GlobalVariableComponent) {
	}	

	ngOnInit(): void {
    	this.getTableData();
  	}

  	getTableData() {
      this.provinceService.searchProvinces().subscribe(result => {
        this.cities = <Province[]> result.json();
      });
  	}

    resetForm() {

      this.isEditing = false;
      this.isSaving = false;
      this.city = null;
    }

    viewRow(rowData: Province) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.city = temp;
      this.displayDialog = true;
    }

    addRow() {
      this.resetForm();
      this.city = new Province(null,'',true);
      this.isSaving = true;
      this.displayDialog = true;
    }

    editRow(rowData: Province) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.city = temp;
      this.isEditing = true;
      this.displayDialog = true;
    }

    closeModal() {
      this.resetForm();
      this.displayDialog = false;
    }

    update() {

      //Validation
      let isValid : boolean = true;

      if(
        this.city.name.trim() == '' 
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.provinceService.saveProvince(this.city)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }

    }

    save() {

      //Validation
      let isValid : boolean = true;

      if(
        this.city.name.trim() == ''
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.provinceService.createProvince(this.city)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Creating Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }      
    }

}

