import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { ProductCreateComponent }   from './product.create.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'product/create', component: ProductCreateComponent}
  ])],
  exports: [RouterModule]
})
export class ProductRouting {}