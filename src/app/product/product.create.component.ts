import { Component, OnInit } from '@angular/core';

import { ProductService } from './product.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Product, ProductCategory } from '../entity/entities';

import {SelectItem,Message} from 'primeng/primeng';

@Component({
	selector: 'product.add',
	templateUrl: './product.create.html',
	providers: [ ProductService ]
})
export class ProductCreateComponent implements OnInit { 

  product : Product;

  msgs : Message[] = [];

	constructor() {
	}	

	ngOnInit(): void {

  }


}

