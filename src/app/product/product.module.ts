import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { ProductCreateComponent }  from './product.create.component';

import { ProductRouting }   from './product.routing';

import { ProductService } from './product.service';

import {ButtonModule,
	InputTextModule,
  GrowlModule,
	FieldsetModule,
  PanelModule,
	TabViewModule,
	DropdownModule,
  DialogModule,
	SharedModule,
	DataTableModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
          FormsModule,
  				ProductRouting,
  				ButtonModule,
  				InputTextModule,
          GrowlModule,
  				FieldsetModule,
          PanelModule,
  				TabViewModule,
  				DropdownModule,
          DialogModule,
  				SharedModule,
  				DataTableModule 
  				],
  declarations: [ ProductCreateComponent],
  providers : [ ProductService ]
})
export class ProductModule {}