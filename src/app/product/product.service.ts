import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Product, Measurement, Location } from '../entity/entities';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class ProductService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchProducts(searchParams : SearchFilterParam[], page : number, perPage : number, sortBy : string, sortType : SortType) {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    params.append("_page",page.toString());
	    params.append("_limit",perPage.toString());

	    if(sortBy!=null) {
	    	params.append("_sort",sortBy.toString());
	    	params.append("_order",sortType.toString());
	    }

	    for (let index in searchParams) {
		    let param : SearchFilterParam = searchParams[index];
		    params.append(param.field,param.value);
		}
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/product', { search: params, headers: this.headers });
	}

	createProduct(product: Product) {

		let body : any = this.toJson(product);

		return this.http.post(this.globalVariable.apiBaseUrl+'/product/',
				body,
				{headers: this.headers}
			);
	}

	saveProduct(product: Product) {

		let body : any = this.toJson(product);

		return this.http.put(this.globalVariable.apiBaseUrl+'/product/'+product.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(product: Product) {
		let body : any = {};
		body.id = product.id;
		body.name = product.name;
		body.isActive = product.isActive;
		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}