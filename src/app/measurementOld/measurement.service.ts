import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Measurement } from '../entity/measurement';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class MeasurementService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchMeasurements(searchParams : SearchFilterParam[], page : number, perPage : number, sortBy : string, sortType : SortType) {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    params.append("_page",page.toString());
	    params.append("_limit",perPage.toString());

	    if(sortBy!=null) {
	    	params.append("_sort",sortBy.toString());
	    	params.append("_order",sortType.toString());
	    }

	    for (let index in searchParams) {
		    let param : SearchFilterParam = searchParams[index];
		    params.append(param.field,param.value);
		}
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/measurementsOld', { search: params, headers: this.headers });
	}

	createMeasurement(measurement: Measurement) {
		console.log(measurement);
		let body : any = this.toJson(measurement);

		return this.http.post(this.globalVariable.apiBaseUrl+'/measurementsOld/',
				body,
				{headers: this.headers}
			);
	}

	saveMeasurement(measurement: Measurement) {
		console.log(measurement);
		let body : any = this.toJson(measurement);

		return this.http.put(this.globalVariable.apiBaseUrl+'/measurementsOld/'+measurement.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(measurement: Measurement) {
		let body : any = {};
		body.id = measurement.id;
		body.name = measurement.name;
		body.type = measurement.type;
		body.abbreviation = measurement.abbreviation;
		body.unit = measurement.unit;
		body.description = measurement.description;
		body.isActive = measurement.isActive;
		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}