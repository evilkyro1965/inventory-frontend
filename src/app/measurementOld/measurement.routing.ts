import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { MeasurementComponent }   from './measurement.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'measurement', component: MeasurementComponent}
  ])],
  exports: [RouterModule]
})
export class MeasurementRouting {}