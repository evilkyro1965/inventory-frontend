import { Component, OnInit } from '@angular/core';

import { MeasurementService } from './measurement.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Measurement, MeasurementType, SearchFilterParam, SortType, SearchCriteria } from '../entity/entities';

import {SelectItem,Message} from 'primeng/primeng';

@Component({
	selector: 'measurement',
	templateUrl: './measurement.component.html',
	providers: [ MeasurementService ]
})
export class MeasurementComponent implements OnInit { 
	searchCriteria : SearchCriteria = new SearchCriteria();

	nameFilter: string = "";
	abbreviationFilter: string = "";
	typeFilter : MeasurementType = null;

  sortFields : SelectItem[] = [];

	measurements: Measurement[];

  collapseFilter : boolean = true;

  displayDialog : boolean = false;
  isSaving : boolean = false;
  isEditing : boolean = false;

  measurement : Measurement;

  typeIsValid : boolean = true;

  msgs : Message[] = [];

	constructor(private measurementService: MeasurementService, public globalVariable : GlobalVariableComponent) {
	}	

	ngOnInit(): void {
    	this.getTableData();
      this.sortFields = [];
      this.sortFields.push({label:'Fields', value:null});
      this.sortFields.push({label:'Name', value:'name'});
      this.sortFields.push({label:'Abbreviation', value:'abbreviation'});
      this.sortFields.push({label:'Type', value:'type'});
  	}

  	getTableData() {
  		this.measurementService.searchMeasurements(this.searchCriteria.searchParams,this.searchCriteria.currPage,
    		this.searchCriteria.perPage,this.searchCriteria.sortBy,this.searchCriteria.sortType)
    		.subscribe(result => {
    			 this.measurements = <Measurement[]> result.json();
    			 this.searchCriteria.totalRow = parseInt(result.headers.get("X-Total-Count"));
    			 this.searchCriteria.totalPage = this.globalVariable.getTotalPage(this.searchCriteria.totalRow,this.searchCriteria.perPage);
    			 this.searchCriteria.arrPage = this.globalVariable.getPageArray(this.searchCriteria.currPage,this.searchCriteria.totalPage);
    		});
  	}

  	getPageData(page:number) {
  		this.searchCriteria.currPage = page;
  		this.getTableData();
  	}

  	filterTableData() {
  		this.searchCriteria.currPage = 1;
  		this.searchCriteria.searchParams = [];

  		if(this.nameFilter!="") {
  			this.searchCriteria.searchParams.push(new SearchFilterParam("name_like",this.nameFilter));
  		}

  		if(this.abbreviationFilter!="") {
  			this.searchCriteria.searchParams.push(new SearchFilterParam("abbreviation_like",this.abbreviationFilter));
  		}

  		if(this.typeFilter!=null) {
  			this.searchCriteria.searchParams.push(new SearchFilterParam("type",this.typeFilter));
  		}
      this.collapseFilter = true;
  		this.getTableData();
  	}

    resetForm() {
      this.typeIsValid = true;
      this.isEditing = false;
      this.isSaving = false;
      this.measurement = null;
    }

    viewRow(rowData: Measurement) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.measurement = temp;
      this.displayDialog = true;
    }

    addRow() {
      this.resetForm();
      this.measurement = new Measurement(null,'',true,'','',null,0.00);
      this.isSaving = true;
      this.displayDialog = true;
    }

    editRow(rowData: Measurement) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.measurement = temp;
      this.isEditing = true;
      this.displayDialog = true;
    }

    closeModal() {
      this.resetForm();
      this.displayDialog = false;
    }

    update() {
      this.typeIsValid = true;

      //Validation
      let isValid : boolean = true;

      if(this.measurement.type==null) {
        isValid = false;
        this.typeIsValid = false;
      }

      if(
        this.measurement.name.trim() == '' ||
        this.measurement.abbreviation.trim() == '' || 
        this.measurement.unit == null
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.measurementService.saveMeasurement(this.measurement)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.filterTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }

    }

    save() {
      this.typeIsValid = true;

      //Validation
      let isValid : boolean = true;

      if(this.measurement.type==null) {
        isValid = false;
        this.typeIsValid = false;
      }

      if(
        this.measurement.name.trim() == '' ||
        this.measurement.abbreviation.trim() == '' || 
        this.measurement.unit == null
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.measurementService.createMeasurement(this.measurement)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.filterTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }      
    }

}

