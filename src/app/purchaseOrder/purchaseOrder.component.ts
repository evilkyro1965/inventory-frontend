import { Component, OnInit } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { PurchaseOrderService } from './purchaseOrder.service';

import { Product, OrderDetails, Measurement, PurchaseOrder, Vendor, ProductSearchCriteria, SearchCriteria } from '../entity/entities';

import { SelectItem, Message } from 'primeng/primeng';

@Component({
	selector: 'purchaseOrder',
	templateUrl: './purchaseOrder.component.html',
	providers: [ PurchaseOrderService ]
})
export class PurchaseOrderComponent implements OnInit { 
	productSearchCriteria : ProductSearchCriteria  = new ProductSearchCriteria();
	products: Product[];

  vendorSearchCriteria : SearchCriteria = new SearchCriteria();
	vendors: Vendor[];

	selectedProduct : Product;
	selectedVendor : Vendor = new Vendor(null,'','','','','','',false);
	orderDetailsList : OrderDetails[] = [];

	orderDetailsModal : OrderDetails;

	displayProductDialog : boolean = false;
	displayVendorDialog : boolean = false;
	isSaving : boolean = false;
	isEditing : boolean = false;
	measurementType : string = "purchase";

	vendor : Vendor = new Vendor(null,'','','','','','',false);
	purchaseDate : Date;
	dueDate : Date;
	orderNo : string;

	freight : number = 0;
  freightStr : string = '0';
	tax : number = 0;
  taxStr : string = '0';
	taxTotal : number = 0;
  taxTotalStr : string = '0';
	total : number = 0;
  totalStr : string = '0';

	/* validation variable */
	vendorValid : boolean = true;
	dateValid : boolean = true;
	noValid : boolean = true;

	msgs : Message[] = [];
  errorMsgs : Message[] = [];

	constructor(private purchaseOrderService: PurchaseOrderService, public globalVariable : GlobalVariableComponent) {

	}	

	ngOnInit(): void {
		let product : Product = new Product(null,'',true);

		for(let i=0;i<5;i++) {
			this.orderDetailsList.push(new OrderDetails(0,0,product,0,'',0,'',true,0,0,'',0,'',0,''));
		}

		this.purchaseOrderService.searchProducts(this.productSearchCriteria).subscribe(result => {
	        this.products = <Product[]> result.json();
          this.productSearchCriteria.totalPage = Number(result.headers.get('X-Total-Page'));
	    });
	    this.purchaseOrderService.searchVendors(this.vendorSearchCriteria).subscribe(result => {
	        this.vendors = <Vendor[]> result.json();
          this.vendorSearchCriteria.totalPage = Number(result.headers.get('X-Total-Page'));
	    });
  	}

  	filterProducts(page : number): void {
      this.productSearchCriteria.page = page;
  		this.purchaseOrderService.searchProducts(this.productSearchCriteria).subscribe(result => {
	        this.products = <Product[]> result.json();
          this.productSearchCriteria.totalPage = Number(result.headers.get('X-Total-Page'));
	    });
  	}

    filterVendors(page : number): void {
      this.vendorSearchCriteria.page = page;
      this.purchaseOrderService.searchVendors(this.vendorSearchCriteria).subscribe(result => {
          this.vendors = <Vendor[]> result.json();
          this.vendorSearchCriteria.totalPage = Number(result.headers.get('X-Total-Page'));
      });
    }

  	showProductsModal(orderDetailsModal:OrderDetails) {
  		this.orderDetailsModal = orderDetailsModal;
  		this.displayProductDialog = true;
  	}

    hideProductsModal() {
      this.displayProductDialog = false;
    }

  	showVendorsModal() {
  		this.displayVendorDialog = true;
  	}

  	hideVendorsModal() {
    	this.displayVendorDialog = false;
    }

  	orderDetailsChange(orderDetails:OrderDetails) {
  		if(orderDetails.quantityUOMStr=='') orderDetails.quantityUOMStr = '0';
  		if(orderDetails.unitPriceStr=='') orderDetails.unitPriceStr = '0';
  		if(orderDetails.discountStr=='') orderDetails.discountStr = '0';

		let qtyUOM = this.globalVariable.stringToNumber(orderDetails.quantityUOMStr)
  		let unitPrice =  this.globalVariable.stringToNumber(orderDetails.unitPriceStr);
  		let discount =  this.globalVariable.stringToNumber(orderDetails.discountStr);

  		if(orderDetails.product.id!=null) {
	  		let qty = qtyUOM * orderDetails.purchaseUOMConversion;
	  		let totalPrice = qtyUOM * unitPrice;
	  		let discountTotal = totalPrice * (discount / 100.0);
	  		let subTotal = totalPrice - discountTotal;

	  		orderDetails.quantityUOM = qtyUOM;
	  		orderDetails.unitPrice = unitPrice;
	  		orderDetails.discount = discount;
	  		orderDetails.quantity = qty;
	  		orderDetails.totalPrice = totalPrice;
	  		orderDetails.discountTotal = discountTotal;
	  		orderDetails.subTotal = subTotal;

	  		orderDetails.subTotalStr = this.globalVariable.numberToString(subTotal);
	  	}
	  	else {
	  		orderDetails.quantityUOM = 0;
	  		orderDetails.purchaseUOMConversion = 1;
	  		orderDetails.quantity = 0;
	  		orderDetails.unitPrice = 0;
	  		orderDetails.totalPrice = 0;
	  		orderDetails.discountTotal = 0;
	  		orderDetails.subTotal = 0;

	  		orderDetails.quantityUOMStr = this.globalVariable.numberToString(qtyUOM);
	  		orderDetails.unitPriceStr = this.globalVariable.numberToString(unitPrice);
	  		orderDetails.discountStr = this.globalVariable.numberToString(discount);
	  		orderDetails.subTotalStr = this.globalVariable.numberToString(orderDetails.subTotal);
	  	}

	  	this.totalOrder();
  	}

  	addRow() {
  		let product : Product = new Product(null,'',true);
  		this.orderDetailsList.push(new OrderDetails(0,0,product,0,'',0,'',true,0,0,'',0,'',0,''));
  	}

  	removeRow(index : number) {
  		this.orderDetailsList.splice(index, 1);
  	}

  	totalOrder() {
  		let total : number = 0;
  		let tax : number = this.globalVariable.stringToNumber(this.taxStr);
  		let taxTotal : number = 0;
  		let freight : number = this.globalVariable.stringToNumber(this.freightStr);

  		for(let orderDetails of this.orderDetailsList) {
  			total += orderDetails.subTotal;
  		}
  		taxTotal = total * (tax / 100.0);
  		total += taxTotal;
  		total += freight;

      this.tax = tax;
      this.freight = freight;
  		this.taxTotal = taxTotal;
  		this.total = total;

      this.taxTotalStr = this.globalVariable.numberToString(this.taxTotal);
      this.totalStr = this.globalVariable.numberToString(this.total);
  	}

  	setOrderDetailsProduct() {

  		if(this.selectedProduct!=null) {

  			this.orderDetailsModal.product = this.selectedProduct;
  			let usePurchaseUOM = this.measurementType == "purchase" ? true : false;
	  		let purchaseUOMConversion = usePurchaseUOM == true ?  this.selectedProduct.purchaseUOMConversion : 1.0; 
	  		this.orderDetailsModal.usePurchaseUOM = usePurchaseUOM;
	  		this.orderDetailsModal.purchaseUOMConversion = purchaseUOMConversion;
	  		this.displayProductDialog = false;
  		}

  	}

  	setVendor() {
  		this.vendor = this.selectedVendor;
  		this.displayVendorDialog = false;
  	}

  	save() {
  		let purchaseOrder : PurchaseOrder = new PurchaseOrder(null);
  		purchaseOrder.no = "123";
  		purchaseOrder.date = this.purchaseDate;
  		purchaseOrder.dueDate = this.dueDate;
  		purchaseOrder.taxPercent = this.tax; 
  		purchaseOrder.freight = this.freight;
  		if(this.vendor.id!=null) {
  			purchaseOrder.vendor = this.vendor;
  		}

  		if(this.validate()) {

	  		this.purchaseOrderService.savePurchase(purchaseOrder, this.orderDetailsList)
	          .subscribe(result => {
	            this.msgs.push({severity:'success', summary:'Success', detail:'Creating Data Success'});
	          },
	          err => {
	            let errorMessage = err.json().message;
	            this.errorMsgs.push({severity:'error', summary:'Error', detail:errorMessage});
	          });
      	}
  	}

  	validate() : boolean {
  		this.vendorValid = true;
  		this.dateValid = true;

  		let ret : boolean = true;

  		if(this.vendor.id == null) {
  			this.vendorValid = false;
  			ret = ret && false;
  		}

  		if(this.purchaseDate == null) {
  			this.dateValid = false;
  			ret = ret && false;
  		}

  		if(this.orderNo == null || this.orderNo == '') {
  			this.noValid = false;
  			ret = ret && false;
  		}

  		return ret;
  	}


}

