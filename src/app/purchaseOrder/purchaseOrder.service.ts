import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Product, OrderDetails, PurchaseOrder } from '../entity/entities';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType, ProductSearchCriteria, SearchCriteria } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class PurchaseOrderService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchProducts(searchCriteria : ProductSearchCriteria) {

		let body : any = {};
		body.page = searchCriteria.page;
		body.limit= searchCriteria.limit;
		body.name = searchCriteria.name;

		return this.http.post(this.globalVariable.apiBaseUrl+'/products/search',
				body,
				{headers: this.headers}
			);
	
	}

	searchVendors(searchCriteria : SearchCriteria) {

		let body : any = {};
		body.page = searchCriteria.page;
		body.limit= searchCriteria.limit;
		body.name = searchCriteria.name;

		return this.http.post(this.globalVariable.apiBaseUrl+'/vendors/search',
				body,
				{headers: this.headers}
			);
	
	}

	getProducts(searchCriteria : ProductSearchCriteria) {
		this.searchProducts(searchCriteria).subscribe(result => {
        	let products = <Product[]> result.json();
			 searchCriteria.totalRow = parseInt(result.headers.get("X-Total-Count"));
			 searchCriteria.totalPage = this.globalVariable.getTotalPage(searchCriteria.totalRow,searchCriteria.limit);
			 searchCriteria.arrPage = this.globalVariable.getPageArray(searchCriteria.page,searchCriteria.totalPage);
     	 });
	}

	savePurchase(purchase: PurchaseOrder, orderDetailsList: OrderDetails[]) {

		let body : any = this.toJson(purchase, orderDetailsList);
		console.log(body);

		return this.http.post(this.globalVariable.apiBaseUrl+'/purchaseOrders/',
				body,
				{headers: this.headers}
			);

	}

	toJson(purchaseOrder: PurchaseOrder, orderDetailsList: OrderDetails[]) {
		let body : any = {};
		body.no = purchaseOrder.no;
		body.date = purchaseOrder.date;
		body.dueDate = purchaseOrder.dueDate;
		body.taxPercent = purchaseOrder.taxPercent;
		body.freight = purchaseOrder.freight;
		body.vendor = purchaseOrder.vendor;

		let orders : any[] = [];
		for(let orderDetails of orderDetailsList) {
			if(orderDetails.product.id!=null) {
				orders.push(this.toJsonOrderDetails(orderDetails));
			}
		}
		body.orders = orders;

		return body;
	}

	toJsonOrderDetails(orderDetails: OrderDetails) {
		let body : any = {};
		body.product = {id:orderDetails.product.id}
		body.quantityUOM = orderDetails.quantityUOM;
		body.usePurchaseUOM = orderDetails.usePurchaseUOM;
		body.purchaseUOMConversion = orderDetails.purchaseUOMConversion;
		body.unitPrice = orderDetails.unitPrice;
		body.discount = orderDetails.discount;
		return body;
	}


}