import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { SharedGlobalModule } from '../shared/shared.module';

import { PurchaseOrderComponent }  from './purchaseOrder.component';

import { PurchaseOrderRouting }   from './purchaseOrder.routing';

import { PurchaseOrderService } from './purchaseOrder.service';

import {ButtonModule,
	InputTextModule,
  GrowlModule,
	FieldsetModule,
  PanelModule,
	TabViewModule,
	DropdownModule,
  DialogModule,
	SharedModule,
	DataTableModule,
  RadioButtonModule,
  CalendarModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
          FormsModule,
          SharedGlobalModule,
  				PurchaseOrderRouting,
  				ButtonModule,
  				InputTextModule,
          GrowlModule,
  				FieldsetModule,
          PanelModule,
  				TabViewModule,
  				DropdownModule,
          DialogModule,
  				SharedModule,
  				DataTableModule,
          RadioButtonModule,
          CalendarModule
  				],
  declarations: [ PurchaseOrderComponent ],
  providers : [ PurchaseOrderService ]
})
export class PUrchaseOrderModule {}