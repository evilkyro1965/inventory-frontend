import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { PurchaseOrderComponent }   from './purchaseOrder.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'purchaseOrder', component: PurchaseOrderComponent}
  ])],
  exports: [RouterModule]
})
export class PurchaseOrderRouting {}