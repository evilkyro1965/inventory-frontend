import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Customer } from '../entity/customer';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';


import 'rxjs/Rx';


@Injectable()
export class CustomerService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	getCustomerName(): string {
		return 'John Doe'+this.globalVariable.restBaseUrl;
	}

	searchCustomers() {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/customers', { search: params, headers: this.headers })
	               .map(response => <Customer[]> response.json()); 
	}

}