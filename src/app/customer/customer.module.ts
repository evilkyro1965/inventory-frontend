import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerComponent }  from './customer.component';

import { CustomerRouting }   from './customer.routing';

import { CustomerService } from './customer.service';

import {ButtonModule,
	InputTextModule,
	FieldsetModule,
	TabViewModule,
	DropdownModule,
	SharedModule,
	DataTableModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
  				CustomerRouting,
  				ButtonModule,
  				InputTextModule,
  				FieldsetModule,
  				TabViewModule,
  				DropdownModule,
  				SharedModule,
  				DataTableModule 
  				],
  declarations: [ CustomerComponent ],
  providers : [ CustomerService ]
})
export class CustomerModule {}