import { Component, OnInit } from '@angular/core';

import { CustomerService } from './customer.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Customer } from '../entity/customer';

import {SelectItem} from 'primeng/primeng';

@Component({
	selector: 'customer',
	templateUrl: './customer.component.html',
	providers: [ CustomerService ]
})
export class CustomerComponent implements OnInit { 
	customerName = '';
	customers: Customer[];
	perPage: number = 5;
	currPage: number = 1;
	pageCount: number;
	selectedCity: string;

	constructor(private customerService: CustomerService, public globalVariable : GlobalVariableComponent) {

	}

	ngOnInit(): void {
    	this.customerName = this.customerService.getCustomerName();
    	this.customerService.searchCustomers().subscribe(result => this.customers = result);
  	}

  	viewCustomer(customer: Customer) {
        console.log(customer);
    }

}

