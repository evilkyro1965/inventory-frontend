import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { LocationComponent }   from './location.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'location', component: LocationComponent}
  ])],
  exports: [RouterModule]
})
export class LocationRouting {}