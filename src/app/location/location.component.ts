import { Component, OnInit } from '@angular/core';

import { LocationService } from './location.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Location } from '../entity/entities';

import { SelectItem, Message } from 'primeng/primeng';

@Component({
	selector: 'location',
	templateUrl: './location.component.html',
	providers: [ LocationService ]
})
export class LocationComponent implements OnInit { 

	locations: Location[];

  displayDialog : boolean = false;
  isSaving : boolean = false;
  isEditing : boolean = false;

  location : Location;

  msgs : Message[] = [];

	constructor(private locationService: LocationService, public globalVariable : GlobalVariableComponent) {
	}	

	ngOnInit(): void {
    	this.getTableData();
  	}

  	getTableData() {
      this.locationService.searchLocations().subscribe(result => {
        this.locations = <Location[]> result.json();
      });
  	}

    resetForm() {

      this.isEditing = false;
      this.isSaving = false;
      this.location = null;
    }

    viewRow(rowData: Location) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.location = temp;
      this.displayDialog = true;
    }

    addRow() {
      this.resetForm();
      this.location = new Location(null,'',true);
      this.isSaving = true;
      this.displayDialog = true;
    }

    editRow(rowData: Location) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.location = temp;
      this.isEditing = true;
      this.displayDialog = true;
    }

    closeModal() {
      this.resetForm();
      this.displayDialog = false;
    }

    update() {

      //Validation
      let isValid : boolean = true;

      if(
        this.location.name.trim() == '' 
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.locationService.saveLocation(this.location)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }

    }

    save() {

      //Validation
      let isValid : boolean = true;

      if(
        this.location.name.trim() == ''
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.locationService.createLocation(this.location)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Creating Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }      
    }

}

