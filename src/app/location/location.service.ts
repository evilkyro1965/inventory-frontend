import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Location } from '../entity/location';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class LocationService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchLocations() {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/locations', { search: params, headers: this.headers });
	}

	createLocation(location: Location) {
		let body : any = this.toJson(location);

		return this.http.post(this.globalVariable.apiBaseUrl+'/locations/',
				body,
				{headers: this.headers}
			);
	}

	saveLocation(location: Location) {
		let body : any = this.toJson(location);

		return this.http.put(this.globalVariable.apiBaseUrl+'/locations/'+location.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(location: Location) {
		let body : any = {};
		body.id = location.id;
		body.name = location.name;
		body.isActive = location.isActive;
		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}