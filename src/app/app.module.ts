import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpModule, JsonpModule } from '@angular/http';

/* Feature Modules */
import { CustomerModule }      from './customer/customer.module';
import { MeasurementModule }      from './measurement/measurement.module';
import { CountryModule }      from './country/country.module';
import { CityModule }      from './city/city.module';
import { ProvinceModule }      from './province/province.module';
import { LocationModule }      from './location/location.module';
import { VendorModule }      from './vendor/vendor.module';
import { ProductModule }      from './product/product.module';
import { PUrchaseOrderModule }      from './purchaseOrder/purchaseOrder.module';
import { PUrchaseOrderUpdateModule }      from './purchaseOrderUpdate/purchaseOrderUpdate.module';
import { PUrchaseOrderReceiveModule }      from './purchaseOrderReceive/purchaseOrderReceive.module';


import { GlobalVariableComponent }   from './shared/global.variable.component';

/* Routing Module */
import { AppRouting }   from './app.routing';

@NgModule({
  imports:      [ 
  					BrowserModule,
  					CustomerModule, 
            MeasurementModule,
            CountryModule,
            CityModule,
            ProvinceModule,
            LocationModule,
            VendorModule,
            ProductModule,
            PUrchaseOrderModule,
            PUrchaseOrderUpdateModule,
            PUrchaseOrderReceiveModule,
  					AppRouting,
  					JsonpModule,
            HttpModule
  					 ],

  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ],
  providers: [GlobalVariableComponent],
})
export class AppModule { }
