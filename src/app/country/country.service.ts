import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Country } from '../entity/country';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class CountryService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchCountries() {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/countries', { search: params, headers: this.headers });
	}

	createCountry(country: Country) {
		let body : any = this.toJson(country);

		return this.http.post(this.globalVariable.apiBaseUrl+'/countries/',
				body,
				{headers: this.headers}
			);
	}

	saveCountry(country: Country) {
		let body : any = this.toJson(country);

		return this.http.put(this.globalVariable.apiBaseUrl+'/countries/'+country.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(country: Country) {
		let body : any = {};
		body.id = country.id;
		body.name = country.name;
		body.isActive = country.isActive;
		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}