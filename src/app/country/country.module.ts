import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { SharedGlobalModule }        from '../shared/shared.module';

import { CountryComponent }  from './country.component';

import { CountryRouting }   from './country.routing';

import { CountryService } from './country.service';

import {ButtonModule,
	InputTextModule,
  GrowlModule,
	FieldsetModule,
  PanelModule,
	TabViewModule,
	DropdownModule,
  DialogModule,
	SharedModule,
	DataTableModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
          FormsModule,
          SharedGlobalModule,
  				CountryRouting,
  				ButtonModule,
  				InputTextModule,
          GrowlModule,
  				FieldsetModule,
          PanelModule,
  				TabViewModule,
  				DropdownModule,
          DialogModule,
  				SharedModule,
  				DataTableModule 
  				],
  declarations: [ CountryComponent ],
  providers : [ CountryService ]
})
export class CountryModule {}