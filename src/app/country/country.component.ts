import { Component, OnInit } from '@angular/core';

import { CountryService } from './country.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Country } from '../entity/entities';

import { SelectItem, Message } from 'primeng/primeng';

@Component({
	selector: 'country',
	templateUrl: './country.component.html',
	providers: [ CountryService ]
})
export class CountryComponent implements OnInit { 

	countries: Country[];

  displayDialog : boolean = false;
  isSaving : boolean = false;
  isEditing : boolean = false;

  country : Country;

  msgs : Message[] = [];

	constructor(private countryService: CountryService, public globalVariable : GlobalVariableComponent) {
	}	

	ngOnInit(): void {
    	this.getTableData();
  	}

  	getTableData() {
      this.countryService.searchCountries().subscribe(result => {
        this.countries = <Country[]> result.json();
      });
  	}

    resetForm() {

      this.isEditing = false;
      this.isSaving = false;
      this.country = null;
    }

    viewRow(rowData: Country) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.country = temp;
      this.displayDialog = true;
    }

    addRow() {
      this.resetForm();
      this.country = new Country(null,'',true);
      this.isSaving = true;
      this.displayDialog = true;
    }

    editRow(rowData: Country) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.country = temp;
      this.isEditing = true;
      this.displayDialog = true;
    }

    closeModal() {
      this.resetForm();
      this.displayDialog = false;
    }

    update() {

      //Validation
      let isValid : boolean = true;

      if(
        this.country.name.trim() == '' 
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.countryService.saveCountry(this.country)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }

    }

    save() {

      //Validation
      let isValid : boolean = true;

      if(
        this.country.name.trim() == ''
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.countryService.createCountry(this.country)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Creating Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }      
    }

}

