import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Measurement } from '../entity/measurement';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class MeasurementService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchMeasurements() {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/measurements', { search: params, headers: this.headers });
	}

	createMeasurement(measurement: Measurement) {
		console.log(measurement);
		let body : any = this.toJson(measurement);

		return this.http.post(this.globalVariable.apiBaseUrl+'/measurements/',
				body,
				{headers: this.headers}
			);
	}

	saveMeasurement(measurement: Measurement) {
		console.log(measurement);
		let body : any = this.toJson(measurement);

		return this.http.put(this.globalVariable.apiBaseUrl+'/measurements/'+measurement.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(measurement: Measurement) {
		let body : any = {};
		body.id = measurement.id;
		body.name = measurement.name;
		body.abbreviation = measurement.abbreviation;
		body.isActive = measurement.isActive;
		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}