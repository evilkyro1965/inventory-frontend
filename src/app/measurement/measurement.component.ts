import { Component, OnInit } from '@angular/core';

import { MeasurementService } from './measurement.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { Measurement } from '../entity/entities';

import { SelectItem, Message } from 'primeng/primeng';

@Component({
	selector: 'measurement',
	templateUrl: './measurement.component.html',
	providers: [ MeasurementService ]
})
export class MeasurementComponent implements OnInit { 

	measurements: Measurement[];

  displayDialog : boolean = false;
  isSaving : boolean = false;
  isEditing : boolean = false;

  measurement : Measurement;

  msgs : Message[] = [];

	constructor(private measurementService: MeasurementService, public globalVariable : GlobalVariableComponent) {
	}	

	ngOnInit(): void {
    	this.getTableData();
  	}

  	getTableData() {
      this.measurementService.searchMeasurements().subscribe(result => {
        this.measurements = <Measurement[]> result.json();
      });
  	}

    resetForm() {

      this.isEditing = false;
      this.isSaving = false;
      this.measurement = null;
    }

    viewRow(rowData: Measurement) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.measurement = temp;
      this.displayDialog = true;
    }

    addRow() {
      this.resetForm();
      this.measurement = new Measurement(null,'','',true);
      this.isSaving = true;
      this.displayDialog = true;
    }

    editRow(rowData: Measurement) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.measurement = temp;
      this.isEditing = true;
      this.displayDialog = true;
    }

    closeModal() {
      this.resetForm();
      this.displayDialog = false;
    }

    update() {

      //Validation
      let isValid : boolean = true;

      if(
        this.measurement.name.trim() == '' ||
        this.measurement.abbreviation.trim() == '' 
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.measurementService.saveMeasurement(this.measurement)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }

    }

    save() {

      //Validation
      let isValid : boolean = true;

      if(
        this.measurement.name.trim() == '' ||
        this.measurement.abbreviation.trim() == '' 
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.measurementService.createMeasurement(this.measurement)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }      
    }

}

