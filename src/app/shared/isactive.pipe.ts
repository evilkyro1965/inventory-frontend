import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isActive'
})
export class IsActivePipe implements PipeTransform {
  transform(value:boolean) {
    if(value) {
    	return "Active";
    }
    return "Not Active";
  }
}