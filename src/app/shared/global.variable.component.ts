/// <reference path="../../../node_modules/@types/accounting/index.d.ts" />

import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import {SelectItem} from 'primeng/primeng';
import { SortType, LookupEntity } from '../entity/entities';
import accounting = require('accounting');


@Injectable()
export class GlobalVariableComponent {

	restBaseUrl : string = 'http://localhost:8080';

	apiBaseUrl : string = 'http://localhost:8080/api';

	restApiHeader : Headers;

	perPageSelect : SelectItem[];

	measurementTypeSelect : SelectItem[] = [];

	sortTypeSelect : SelectItem[] = [];

	disableSelect : SelectItem[] = []; 

	constructor() { 

		this.restApiHeader = new Headers();
		this.restApiHeader.append('Content-Type', 'application/json');

		this.perPageSelect = [];
        this.perPageSelect.push({label:'Post Per Page', value:null});
        this.perPageSelect.push({label:'5', value:5});
        this.perPageSelect.push({label:'10', value:10});
        this.perPageSelect.push({label:'20', value:20});
        this.perPageSelect.push({label:'50', value:50});

        this.measurementTypeSelect = [];
        this.measurementTypeSelect.push({label:'Type', value:null});
		this.measurementTypeSelect.push({label:'Stock', value:"STOCK"});
		this.measurementTypeSelect.push({label:'Purchase', value:"PURCHASE"});

		this.sortTypeSelect = [];
        this.sortTypeSelect.push({label:'Sort Type', value:null});
		this.sortTypeSelect.push({label:'Ascending', value:"asc"});
		this.sortTypeSelect.push({label:'Descending', value:"desc"});

		this.disableSelect = [];
        this.disableSelect.push({label:'Active', value:true});
		this.disableSelect.push({label:'Disabled', value:false});
	}

	getRestApiHeader() {
		return this.restApiHeader;
	}
  
  	getRestParams() : URLSearchParams {
  		let restParams : URLSearchParams = new URLSearchParams();
		return restParams;
	}

	getTotalPage(totalRow:number, perPage:number) {
		var totalPage = totalRow / perPage;
		totalPage += totalRow % perPage != 0 ? 1: 0;
		return totalPage;
	}

	getPageArray(currPage:number,totalPage:number) {
		var pages : number[] = [];
		var startPage = (((currPage / 5)|0) * 5);
		startPage += startPage == 0 ? 1 : 0;
		var endPage = startPage + 5;
		endPage = endPage > totalPage ? totalPage : endPage;

		for(var i=startPage;i<=endPage;i++) {
			pages.push(i);
		}
		return pages;
	}

	entityToSelectItems(datas: LookupEntity[], defaultLabel: string) {
		let selectItems : SelectItem[] = [];
		selectItems.push({label:defaultLabel, value:null});
		for (var data of datas) {
		    selectItems.push({label:data.name, value:data.id});
		}
		return selectItems;
	}

	stringToNumber(str : string) : number {
		let ret = 0;
		str = str.replace('.','');
		str = str.replace(',','.');
		ret = Number(str);
		return ret;
	}

	numberToString(number : number) : string {
		let ret = '0';
		ret = accounting.formatMoney(number, "", 2, ".", ",");
		return ret;
	}


}