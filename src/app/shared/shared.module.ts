import { NgModule, ModuleWithProviders }       from '@angular/core';
import { CommonModule }   from '@angular/common';

import { FillPipe }   from '../shared/fillpipe';
import { IsActivePipe }   from '../shared/isactive.pipe';
import { CurrencyFormatterDirective }   from '../shared/CurrencyFormatterDirective';

@NgModule({
  imports:      [ CommonModule ],
  declarations: [ FillPipe, IsActivePipe, CurrencyFormatterDirective ],
  exports:      [ FillPipe, IsActivePipe, CurrencyFormatterDirective,
                  CommonModule ]
})
export class SharedGlobalModule { }