import { Directive, HostListener, ElementRef, OnInit } from "@angular/core";

@Directive({ selector: "[CurrencyFormatter]" })
export class CurrencyFormatterDirective implements OnInit {

  private el: HTMLInputElement;
  private decimalSep : string = ',';
  private thousandSep : string = '.';
  private decimalLength : number = 2;


  constructor(
    private elementRef: ElementRef
  ) {
    this.el = this.elementRef.nativeElement;
  }

  ngOnInit() {
  }

  @HostListener("focus", ["$event.target.value"])
  onFocus(value:any) {
  }

  @HostListener("blur", ["$event.target.value"])
  onBlur(value:any) {
  }

  @HostListener("keyup", ["$event.target.value"])
  onKeyUp(value:any) {
    var filterExp ='^[0-9|\.|\,]+$';

    var reg = new RegExp(filterExp);
    var inputText = value;
    var lastInput = inputText.substring(0, inputText.length-1);

    var key = inputText.substring(inputText.length-1,inputText.length);
    var isAfterComma = false;

    var isNumber = reg.test(inputText);

    if(!isNumber) {
      inputText = inputText.substring(0, inputText.length-1);
    }
    else if( 
      (key==this.decimalSep && inputText.length == 1) || 
      (key==this.decimalSep && lastInput.indexOf(this.decimalSep) != -1) 
    ) {
      inputText = inputText.substring(0, inputText.length-1);
    }

    isAfterComma = inputText.indexOf(this.decimalSep) != -1 && this.decimalSep != '' ? true : false;

    //If using thousand sep
    if(this.thousandSep!='') {
      //Add thousand
      var inputFormated = "";
      var j = 0;
      inputText = inputText.split(this.thousandSep).join('');
      var afterComma = '';
      if(isAfterComma) {
        var commaIndex = inputText.indexOf(this.decimalSep);
        afterComma = inputText.substring(commaIndex,inputText.length);
        if(afterComma.length > this.decimalLength + 1) {
          afterComma = afterComma.substring(0,afterComma.length-1);
        }
        inputText = inputText.substring(0,commaIndex);
      }

      for(var i=inputText.length-1;i>=0;i--) {
        var temp = inputText.charAt(i);

        inputFormated = temp + inputFormated;
        if( j>0 && j%2 == 0 && i > 0 ) {
          inputFormated = this.thousandSep + inputFormated;
          j = 0;
          continue;
        }
        j++;
      }

      //Merge decimal with thousand
      if(isAfterComma && inputText.length>0) {
        inputFormated = inputFormated + afterComma;
      }

      this.el.value = inputFormated;
    }
    else {
      this.el.value = inputText;
    }

    
  }


}
