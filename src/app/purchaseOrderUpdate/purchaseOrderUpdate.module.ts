import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { SharedGlobalModule } from '../shared/shared.module';

import { PurchaseOrderUpdateComponent }  from './purchaseOrderUpdate.component';

import { PurchaseOrderUpdateRouting }   from './purchaseOrderUpdate.routing';

import { PurchaseOrderUpdateService } from './purchaseOrderUpdate.service';

import {ButtonModule,
	InputTextModule,
  GrowlModule,
	FieldsetModule,
  PanelModule,
	TabViewModule,
	DropdownModule,
  DialogModule,
	SharedModule,
	DataTableModule,
  RadioButtonModule,
  CalendarModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
          FormsModule,
          SharedGlobalModule,
  				PurchaseOrderUpdateRouting,
  				ButtonModule,
  				InputTextModule,
          GrowlModule,
  				FieldsetModule,
          PanelModule,
  				TabViewModule,
  				DropdownModule,
          DialogModule,
  				SharedModule,
  				DataTableModule,
          RadioButtonModule,
          CalendarModule
  				],
  declarations: [ PurchaseOrderUpdateComponent ],
  providers : [ PurchaseOrderUpdateService ]
})
export class PUrchaseOrderUpdateModule {}