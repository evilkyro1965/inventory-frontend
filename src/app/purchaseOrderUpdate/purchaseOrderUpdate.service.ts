import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Product, OrderDetails, PurchaseOrder } from '../entity/entities';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType, ProductSearchCriteria, SearchCriteria } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class PurchaseOrderUpdateService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	getPurchaseOrder(id : number) {

		return this.http.get(this.globalVariable.apiBaseUrl+'/purchaseOrders/'+id,
				{headers: this.headers}
			);
	
	}

	searchProducts(searchCriteria : ProductSearchCriteria) {

		let body : any = {};
		body.page = searchCriteria.page;
		body.limit= searchCriteria.limit;
		body.name = searchCriteria.name;

		return this.http.post(this.globalVariable.apiBaseUrl+'/products/search',
				body,
				{headers: this.headers}
			);
	
	}

	searchVendors(searchCriteria : SearchCriteria) {

		let body : any = {};
		body.page = searchCriteria.page;
		body.limit= searchCriteria.limit;
		body.name = searchCriteria.name;

		return this.http.post(this.globalVariable.apiBaseUrl+'/vendors/search',
				body,
				{headers: this.headers}
			);
	
	}

	getProducts(searchCriteria : ProductSearchCriteria) {
		this.searchProducts(searchCriteria).subscribe(result => {
        	let products = <Product[]> result.json();
			 searchCriteria.totalRow = parseInt(result.headers.get("X-Total-Count"));
			 searchCriteria.totalPage = this.globalVariable.getTotalPage(searchCriteria.totalRow,searchCriteria.limit);
			 searchCriteria.arrPage = this.globalVariable.getPageArray(searchCriteria.page,searchCriteria.totalPage);
     	 });
	}

	savePurchase(purchase: PurchaseOrder, orderDetailsList: OrderDetails[]) {
		console.log(purchase);

		let body : any = this.toJson(purchase, orderDetailsList);
		
		console.log(body);

		return this.http.put(this.globalVariable.apiBaseUrl+'/purchaseOrders/'+purchase.id,
				body,
				{headers: this.headers}
			);

	}

	toJson(purchaseOrder: PurchaseOrder, orderDetailsList: OrderDetails[]) {
		let body : any = {};
		body.id = purchaseOrder.id;
		body.no = purchaseOrder.no;
		body.date = purchaseOrder.date;
		body.dueDate = purchaseOrder.dueDate;
		body.taxPercent = purchaseOrder.taxPercent;
		body.freight = purchaseOrder.freight;
		body.vendor = purchaseOrder.vendor;
		body.paid = purchaseOrder.paid;
		body.closed = purchaseOrder.closed;
		body.fulfilled = purchaseOrder.fulfilled;

		let orders : any[] = [];
		for(let orderDetails of orderDetailsList) {
			if(orderDetails.product.id!=null) {
				orders.push(this.toJsonOrderDetails(orderDetails));
			}
		}
		body.orders = orders;

		return body;
	}

	toJsonOrderDetails(orderDetails: OrderDetails) {
		let body : any = {};
		body.id = orderDetails.id;
		body.product = {id:orderDetails.product.id}
		body.quantityUOM = orderDetails.quantityUOM;
		body.usePurchaseUOM = orderDetails.usePurchaseUOM;
		body.purchaseUOMConversion = orderDetails.purchaseUOMConversion;
		body.unitPrice = orderDetails.unitPrice;
		body.discount = orderDetails.discount;
		return body;
	}


}