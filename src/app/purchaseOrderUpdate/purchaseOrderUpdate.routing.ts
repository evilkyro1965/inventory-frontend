import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { PurchaseOrderUpdateComponent }   from './purchaseOrderUpdate.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'purchaseOrder/:id', component: PurchaseOrderUpdateComponent}
  ])],
  exports: [RouterModule]
})
export class PurchaseOrderUpdateRouting {}