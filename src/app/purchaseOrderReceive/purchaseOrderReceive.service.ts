import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Product, OrderDetails, ReceiveDetails, PurchaseOrder } from '../entity/entities';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType, ProductSearchCriteria, SearchCriteria } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class PurchaseOrderReceiveService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	getPurchaseOrder(id : number) {

		return this.http.get(this.globalVariable.apiBaseUrl+'/purchaseOrders/'+id,
				{headers: this.headers}
			);
	
	}

	searchProducts(searchCriteria : ProductSearchCriteria) {

		let body : any = {};
		body.page = searchCriteria.page;
		body.limit= searchCriteria.limit;
		body.name = searchCriteria.name;

		return this.http.post(this.globalVariable.apiBaseUrl+'/products/search',
				body,
				{headers: this.headers}
			);
	
	}

	searchVendors(searchCriteria : SearchCriteria) {

		let body : any = {};
		body.page = searchCriteria.page;
		body.limit= searchCriteria.limit;
		body.name = searchCriteria.name;

		return this.http.post(this.globalVariable.apiBaseUrl+'/vendors/search',
				body,
				{headers: this.headers}
			);
	
	}

	searchLocations() {

		return this.http.get(this.globalVariable.apiBaseUrl+'/locations',
				{headers: this.headers}
			);
	
	}

	getProducts(searchCriteria : ProductSearchCriteria) {
		this.searchProducts(searchCriteria).subscribe(result => {
        	let products = <Product[]> result.json();
			 searchCriteria.totalRow = parseInt(result.headers.get("X-Total-Count"));
			 searchCriteria.totalPage = this.globalVariable.getTotalPage(searchCriteria.totalRow,searchCriteria.limit);
			 searchCriteria.arrPage = this.globalVariable.getPageArray(searchCriteria.page,searchCriteria.totalPage);
     	 });
	}

	save(purchase: PurchaseOrder, receiveDetailsList: ReceiveDetails[]) {
		console.log(purchase);

		let body : any = [];
		for(let receiveDetails of receiveDetailsList) {
			body.push(this.toJsonReceiveDetails(receiveDetails));
		}
		
		console.log(body);

		return this.http.put(this.globalVariable.apiBaseUrl+'/purchaseReceives/'+purchase.id,
				body,
				{headers: this.headers}
			);

	}

	toJsonReceiveDetails(receiveDetails: ReceiveDetails) {
		let quantityUOM : number = this.globalVariable.stringToNumber(receiveDetails.quantityUOMStr);

		let body : any = {};
		body.id = receiveDetails.id;
		body.product = {id:receiveDetails.product.id}
		body.location = {id:receiveDetails.location.id}
		body.orderDetails = ({id:receiveDetails.orderDetails.id});
		body.quantityUOM = quantityUOM;
		body.receiveDate = receiveDetails.receiveDate;
		body.quantity = quantityUOM * receiveDetails.purchaseUOMConversion;
		body.usePurchaseUOM = receiveDetails.usePurchaseUOM;
		body.purchaseUOMConversion = receiveDetails.purchaseUOMConversion;

		return body;
	}


}