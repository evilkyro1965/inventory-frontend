import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { PurchaseOrderReceiveComponent }   from './purchaseOrderReceive.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'purchaseReceive/:id', component: PurchaseOrderReceiveComponent}
  ])],
  exports: [RouterModule]
})
export class PurchaseOrderReceiveRouting {}