import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { PurchaseOrderReceiveService } from './purchaseOrderReceive.service';

import { Product, OrderDetails, ReceiveDetails, Measurement, PurchaseOrder, Vendor, ProductSearchCriteria, SearchCriteria, Location } from '../entity/entities';

import { SelectItem, Message } from 'primeng/primeng';

@Component({
	selector: 'purchaseOrderReceive',
	templateUrl: './purchaseOrderReceive.component.html',
	providers: [ PurchaseOrderReceiveService ]
})
export class PurchaseOrderReceiveComponent implements OnInit { 
  purchaseId : number;
  purchaseOrder : PurchaseOrder;

	productSearchCriteria : ProductSearchCriteria  = new ProductSearchCriteria();
	products: Product[];

  locations : Location[];

	selectedProduct : Product;
  selectedLocation : Location = new Location(null,'',false);

	receiveDetailsList : ReceiveDetails[] = [];
  orderDetailsList : OrderDetails[] = [];

	orderDetailsModal : OrderDetails;
  receiveDetailsModal : ReceiveDetails;

  	displayProductDialog : boolean = false;
  	displayLocationDialog : boolean = false;
  	isSaving : boolean = false;
  	isEditing : boolean = false;
  	measurementType : string = "purchase";

  	vendor : Vendor = new Vendor(null,'','','','','','',false);
  	purchaseDate : Date;
  	dueDate : Date;
  	orderNo : string;

  	freight : number = 0;
    freightStr : string = '0';
  	tax : number = 0;
    taxStr : string = '0';
  	taxTotal : number = 0;
    taxTotalStr : string = '0';
  	total : number = 0;
    totalStr : string = '0';

  	/* validation variable */
  	vendorValid : boolean = true;
  	dateValid : boolean = true;
  	noValid : boolean = true;

  	msgs : Message[] = [];
    errorMsgs : Message[] = [];

	constructor(private route: ActivatedRoute, private purchaseOrderService: PurchaseOrderReceiveService, public globalVariable : GlobalVariableComponent) {

	}	

	ngOnInit(): void {
		let product : Product = new Product(null,'',true);

    this.route.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.purchaseId = id;
        this.purchaseOrderService.getPurchaseOrder(this.purchaseId).subscribe(result => {
          let purchaseOrder : PurchaseOrder = <PurchaseOrder> result.json();
          this.purchaseOrder = purchaseOrder;
          this.setInputFromPurchaseOrder();
        });    
      });

		this.purchaseOrderService.searchProducts(this.productSearchCriteria).subscribe(result => {
	        this.products = <Product[]> result.json();
          this.productSearchCriteria.totalPage = Number(result.headers.get('X-Total-Page'));
	    });
	    this.purchaseOrderService.searchLocations().subscribe(result => {
	        this.locations = <Location[]> result.json();
	    });
  	}

  	filterProducts(page : number): void {
      this.productSearchCriteria.page = page;
  		this.purchaseOrderService.searchProducts(this.productSearchCriteria).subscribe(result => {
	        this.products = <Product[]> result.json();
          this.productSearchCriteria.totalPage = Number(result.headers.get('X-Total-Page'));
	    });
  	}

  	showProductsModal(orderDetailsModal:OrderDetails) {
  		this.orderDetailsModal = orderDetailsModal;
  		this.displayProductDialog = true;
  	}

    showLocationsModal(receiveDetailsModal : ReceiveDetails) {
      this.receiveDetailsModal = receiveDetailsModal;
      this.displayLocationDialog = true;
    }

    hideProductsModal() {
      this.displayProductDialog = false;
    }

  	hideLocationsModal() {
    	this.displayLocationDialog = false;
    }

  	orderDetailsChange(orderDetails:OrderDetails) {
  		if(orderDetails.quantityUOMStr=='') orderDetails.quantityUOMStr = '0';
  		if(orderDetails.unitPriceStr=='') orderDetails.unitPriceStr = '0';
  		if(orderDetails.discountStr=='') orderDetails.discountStr = '0';

		let qtyUOM = this.globalVariable.stringToNumber(orderDetails.quantityUOMStr)
  		let unitPrice =  this.globalVariable.stringToNumber(orderDetails.unitPriceStr);
  		let discount =  this.globalVariable.stringToNumber(orderDetails.discountStr);

  		if(orderDetails.product.id!=null) {
	  		let qty = qtyUOM * orderDetails.purchaseUOMConversion;
	  		let totalPrice = qtyUOM * unitPrice;
	  		let discountTotal = totalPrice * (discount / 100.0);
	  		let subTotal = totalPrice - discountTotal;

	  		orderDetails.quantityUOM = qtyUOM;
	  		orderDetails.unitPrice = unitPrice;
	  		orderDetails.discount = discount;
	  		orderDetails.quantity = qty;
	  		orderDetails.totalPrice = totalPrice;
	  		orderDetails.discountTotal = discountTotal;
	  		orderDetails.subTotal = subTotal;

	  		orderDetails.subTotalStr = this.globalVariable.numberToString(subTotal);
	  	}
	  	else {
	  		orderDetails.quantityUOM = 0;
	  		orderDetails.purchaseUOMConversion = 1;
	  		orderDetails.quantity = 0;
	  		orderDetails.unitPrice = 0;
	  		orderDetails.totalPrice = 0;
	  		orderDetails.discountTotal = 0;
	  		orderDetails.subTotal = 0;

	  		orderDetails.quantityUOMStr = this.globalVariable.numberToString(qtyUOM);
	  		orderDetails.unitPriceStr = this.globalVariable.numberToString(unitPrice);
	  		orderDetails.discountStr = this.globalVariable.numberToString(discount);
	  		orderDetails.subTotalStr = this.globalVariable.numberToString(orderDetails.subTotal);
	  	}

	  	this.totalOrder();
  	}

    setInputFromPurchaseOrder() {
      this.vendor = this.purchaseOrder.vendor;
      this.dueDate = new Date(this.purchaseOrder.dueDate);
      this.purchaseDate = new Date(this.purchaseOrder.date);
      this.orderNo = this.purchaseOrder.no;

      this.freight = this.purchaseOrder.freight;
      this.freightStr = this.globalVariable.numberToString(this.purchaseOrder.freight);

      this.tax = this.purchaseOrder.taxPercent;
      this.taxStr = this.globalVariable.numberToString(this.purchaseOrder.taxPercent);

      this.total = this.purchaseOrder.total;
      this.totalStr = this.globalVariable.numberToString(this.purchaseOrder.total); 

      this.orderDetailsList = this.purchaseOrder.orders;
      for(let orderDetails of this.orderDetailsList) {

        let location : Location = new Location(null,'',false);

        let receiveDetails : ReceiveDetails = orderDetails.receiveDetails;

        if(receiveDetails==null) {
          receiveDetails = new ReceiveDetails(null, this.purchaseId, orderDetails.product, location, orderDetails, 0, '0', 0,
            orderDetails.usePurchaseUOM, orderDetails.purchaseUOMConversion, new Date(),
            orderDetails.quantityUOM, this.globalVariable.numberToString(orderDetails.quantityUOM) );
            receiveDetails.orderDetails = orderDetails;          
        }
        else {
          receiveDetails.orderDetails = orderDetails;
          receiveDetails.quantityUOMStr = this.globalVariable.numberToString(receiveDetails.quantityUOM);
          receiveDetails.orderQty = orderDetails.quantityUOM;
          receiveDetails.orderQtyStr = this.globalVariable.numberToString(orderDetails.quantityUOM); 
        }
        

        this.receiveDetailsList.push(receiveDetails);
      }
    }

  	addRow() {
  		let product : Product = new Product(null,'',true);
  		this.orderDetailsList.push(new OrderDetails(0,0,product,0,'',0,'',true,0,0,'',0,'',0,''));
  	}

  	removeRow(index : number) {
  		this.orderDetailsList.splice(index, 1);
  	}

  	totalOrder() {
  		let total : number = 0;
  		let tax : number = this.globalVariable.stringToNumber(this.taxStr);
  		let taxTotal : number = 0;
  		let freight : number = this.globalVariable.stringToNumber(this.freightStr);

  		for(let orderDetails of this.orderDetailsList) {
  			total += orderDetails.subTotal;
  		}
  		taxTotal = total * (tax / 100.0);
  		total += taxTotal;
  		total += freight;

      this.tax = tax;
      this.freight = freight;
  		this.taxTotal = taxTotal;
  		this.total = total;

      this.taxTotalStr = this.globalVariable.numberToString(this.taxTotal);
      this.totalStr = this.globalVariable.numberToString(this.total);
  	}

    setReceiveDetailsLocation() {

      if(this.selectedLocation!=null) {

        this.receiveDetailsModal.location = this.selectedLocation;
        this.displayLocationDialog = false;
      }

    }

  	setOrderDetailsProduct() {

  		if(this.selectedProduct!=null) {

  			this.orderDetailsModal.product = this.selectedProduct;
  			let usePurchaseUOM = this.measurementType == "purchase" ? true : false;
	  		let purchaseUOMConversion = usePurchaseUOM == true ?  this.selectedProduct.purchaseUOMConversion : 1.0; 
	  		this.orderDetailsModal.usePurchaseUOM = usePurchaseUOM;
	  		this.orderDetailsModal.purchaseUOMConversion = purchaseUOMConversion;
	  		this.displayProductDialog = false;
  		}

  	}

  	save() {

  		if(this.validate()) {

	  		this.purchaseOrderService.save(this.purchaseOrder,this.receiveDetailsList)
	          .subscribe(result => {
	            this.msgs.push({severity:'success', summary:'Success', detail:'Creating Data Success'});
	          },
	          err => {
	            let errorMessage = err.json().message;
	            this.errorMsgs.push({severity:'error', summary:'Error', detail:errorMessage});
	          });
      	}
  	}

  	validate() : boolean {
  		this.vendorValid = true;
  		this.dateValid = true;

  		let ret : boolean = true;

  		if(this.vendor.id == null) {
  			this.vendorValid = false;
  			ret = ret && false;
  		}

  		if(this.purchaseDate == null) {
  			this.dateValid = false;
  			ret = ret && false;
  		}

  		if(this.orderNo == null || this.orderNo == '') {
  			this.noValid = false;
  			ret = ret && false;
  		}

  		return ret;
  	}


}

