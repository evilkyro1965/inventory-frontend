import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { SharedGlobalModule } from '../shared/shared.module';

import { PurchaseOrderReceiveComponent }  from './purchaseOrderReceive.component';

import { PurchaseOrderReceiveRouting }   from './purchaseOrderReceive.routing';

import { PurchaseOrderReceiveService } from './purchaseOrderReceive.service';

import {ButtonModule,
	InputTextModule,
  GrowlModule,
	FieldsetModule,
  PanelModule,
	TabViewModule,
	DropdownModule,
  DialogModule,
	SharedModule,
	DataTableModule,
  RadioButtonModule,
  CalendarModule
} from 'primeng/primeng';

@NgModule({
  imports:      [ CommonModule, 
          FormsModule,
          SharedGlobalModule,
  				PurchaseOrderReceiveRouting,
  				ButtonModule,
  				InputTextModule,
          GrowlModule,
  				FieldsetModule,
          PanelModule,
  				TabViewModule,
  				DropdownModule,
          DialogModule,
  				SharedModule,
  				DataTableModule,
          RadioButtonModule,
          CalendarModule
  				],
  declarations: [ PurchaseOrderReceiveComponent ],
  providers : [ PurchaseOrderReceiveService ]
})
export class PUrchaseOrderReceiveModule {}