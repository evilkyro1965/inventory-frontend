import { Component, OnInit } from '@angular/core';

import { CityService } from './city.service';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { Observable }       from 'rxjs/Observable';

import { City } from '../entity/entities';

import { SelectItem, Message } from 'primeng/primeng';

@Component({
	selector: 'city',
	templateUrl: './city.component.html',
	providers: [ CityService ]
})
export class CityComponent implements OnInit { 

	cities: City[];

  displayDialog : boolean = false;
  isSaving : boolean = false;
  isEditing : boolean = false;

  city : City;

  msgs : Message[] = [];

	constructor(private cityService: CityService, public globalVariable : GlobalVariableComponent) {
	}	

	ngOnInit(): void {
    	this.getTableData();
  	}

  	getTableData() {
      this.cityService.searchCities().subscribe(result => {
        this.cities = <City[]> result.json();
      });
  	}

    resetForm() {

      this.isEditing = false;
      this.isSaving = false;
      this.city = null;
    }

    viewRow(rowData: City) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.city = temp;
      this.displayDialog = true;
    }

    addRow() {
      this.resetForm();
      this.city = new City(null,'',true);
      this.isSaving = true;
      this.displayDialog = true;
    }

    editRow(rowData: City) {
      this.resetForm();
      var temp:any=Object.create(rowData);
      this.city = temp;
      this.isEditing = true;
      this.displayDialog = true;
    }

    closeModal() {
      this.resetForm();
      this.displayDialog = false;
    }

    update() {

      //Validation
      let isValid : boolean = true;

      if(
        this.city.name.trim() == '' 
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.cityService.saveCity(this.city)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Editing Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }

    }

    save() {

      //Validation
      let isValid : boolean = true;

      if(
        this.city.name.trim() == ''
        )
      {
       isValid = false; 
      }  

      if(isValid) {

        this.cityService.createCity(this.city)
          .subscribe(result => {
            this.msgs.push({severity:'success', summary:'Success', detail:'Creating Data Success'});
            this.resetForm();
            this.displayDialog = false;
            this.getTableData();
          });

        setTimeout(()=>{
          this.msgs=[];
        }, 1000);  
      }      
    }

}

