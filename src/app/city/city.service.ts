import { Injectable } from '@angular/core';

import { GlobalVariableComponent }   from '../shared/global.variable.component';

import { City } from '../entity/city';

import { Http, Response, URLSearchParams, Headers } from '@angular/http';

import {Observable}     from 'rxjs/Observable';

import { SearchFilterParam, SortType } from '../entity/entities';


import 'rxjs/Rx';


@Injectable()
export class CityService {

	headers: Headers;

	constructor(public globalVariable : GlobalVariableComponent, private http: Http) { 
		this.headers = this.globalVariable.getRestApiHeader();
	}

	searchCities() {
		
	    let params : URLSearchParams = this.globalVariable.getRestParams();
	    
	    // TODO: Add error handling
	    return this.http
	               .get(this.globalVariable.apiBaseUrl+'/cities', { search: params, headers: this.headers });
	}

	createCity(city: City) {
		let body : any = this.toJson(city);

		return this.http.post(this.globalVariable.apiBaseUrl+'/cities/',
				body,
				{headers: this.headers}
			);
	}

	saveCity(city: City) {
		let body : any = this.toJson(city);

		return this.http.put(this.globalVariable.apiBaseUrl+'/cities/'+city.id,
				body,
				{headers: this.headers}
			);
	}

	toJson(city: City) {
		let body : any = {};
		body.id = city.id;
		body.name = city.name;
		body.isActive = city.isActive;
		body.createdBy = "";
		body.createdDate = new Date();
		body.updatedBy = "";
		body.updatedDate = new Date();

		return body;
	}

}